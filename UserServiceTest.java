public class UserServiceTest{
    private UserDB mock;
    private UserService service;
    

    @BeforeEach
    public void SetUp(){
        mock = Mockito.mock(UserDB.class);
        service = new service(mock);
    }

    @Test
    void ChangePasswordMustReturnTrueIfPasswordLenghtIs8orBigger(){
        UserDB = new UserDB("GeorgeGaroufalis","123456789");
        when(mock.changepassword(UserDB.getUsername(),UserDB.getPassword())).thenReturn(true);
        assertEquals(true,service.changepassword(UserDB.getUsername(),UserDB.getPassword()));
    }

    @Test
    void ChangePasswordMustReturnFalseIfPasswordLenghtIs7orSmaller(){
        UserDB = new UserDB("GeorgeGaroufalis","1234");
        when(mock.changepassword(UserDB.getUsername(),UserDB.getPassword())).thenReturn(false);
        assertEquals(false,service.changepassword(UserDB.getUsername(),UserDB.getPassword()));
}